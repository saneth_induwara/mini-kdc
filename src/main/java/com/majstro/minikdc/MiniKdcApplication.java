package com.majstro.minikdc;

import org.apache.commons.io.FileUtils;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.kerberos.test.MiniKdc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
public class MiniKdcApplication {


    private static final String KRB_WORK_DIR = "etc/kerberos";

    public static void main(String[] args) throws Exception {

        String[] config = MiniKdcConfigBuilder.builder()
                .workDir(prepareWorkDir())
                .confDir("minikdc-krb5.conf")
                .keytabName("example.keytab")
                .principals("client/localhost", "HTTP/localhost", "arm1", "arm2", "approver1",
                        "approver2", "reviewer1", "reviewer2")
                .build();

        MiniKdc.main(config);
    }

    private static String prepareWorkDir() throws IOException {
        Path dir = Paths.get(KRB_WORK_DIR);
        File directory = dir.normalize().toFile();

        FileUtils.deleteQuietly(directory);
        FileUtils.forceMkdir(directory);
        return dir.toString();
    }
}
